from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
from pykml import parser
import simplekml
import cv2
from matplotlib import pyplot as plt
import numpy as np
import gdal
from pyproj import Proj, transform
from skimage import io, img_as_float
from skimage.io import imread
from matplotlib import pyplot as plt

from skimage.exposure import cumulative_distribution
from pathlib import Path

cv2.useOptimized()

import pandas as pd


def set_crs(x, y):
    inProj = Proj(init='epsg:32615')
    outProj = Proj(init='epsg:4326')
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2



def world2Pixel(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    rtnX = geoMatrix[2]
    rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return (pixel, line)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


def cdf(im):
 '''
 computes the CDF of an image im as 2D numpy ndarray
 '''
 c, b = cumulative_distribution(im)
 # pad the beginning and ending pixels and their CDF values
 c = np.insert(c, 0, [0]*b[0])
 c = np.append(c, [1]*(255-b[-1]))
 return c


def hist_matching(c, c_t, im):
 '''
 c: CDF of input image computed with the function cdf()
 c_t: CDF of template image computed with the function cdf()
 im: input image as 2D numpy ndarray
 returns the modified pixel values
 '''
 pixels = np.arange(256)
 # find closest pixel-matches corresponding to the CDF of the input image, given the value of the CDF H of
 # the template image at the corresponding pixels, s.t. c_t = H(pixels) <=> pixels = H-1(c_t)
 new_pixels = np.interp(c, c_t, pixels)
 im = (np.reshape(new_pixels[im.ravel()], im.shape)).astype(np.uint8)
 return im

# =================================== Reading the Image and KML ==========================================

file_paths=['/test_container/Orthos/090818.tif',
            '/test_container/Orthos/091118.tif',
            '/test_container/Orthos/091418.tif',
            '/test_container/Orthos/091718.tif',
            '/test_container/Orthos/091918.tif',
            '/test_container/Orthos/092218.tif',
            '/test_container/Orthos/092518.tif',
            '/test_container/Orthos/092618.tif',
            '/test_container/Orthos/092818.tif',
            '/test_container/Orthos/100218.tif',
            '/test_container/Orthos/100418.tif']


date_nos =['08/09/18',
           '11/09/18',
           '14/09/18',
           '17/09/18',
           '19/09/18',
           '22/09/18',
           '25/09/18',
           '26/09/18',
           '28/09/18',
           '02/10/18',
           '04/10/18'
           ]




for it in range(len(file_paths)):

    img = imread(file_paths[it])


    if it == 0:
        kml_file = "/test_container/Source_CAD_KML/final_grid_updated.kml"
    else:
        v = Path(file_paths[it-1]).name
        file = v.replace(".", " ").split()[0]
        kml_file = '/test_container/Source_KMLS/'+file+'_rem.kml'

    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder
    except:
        doc = docs.getroot().Document
    print('number of features in KML is', len(doc.Placemark))


    # ===================================== Load KML and append the Data in variable ============================

    ID = []
    coords = []
    for place in doc.Placemark:
        x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        if '\n' not in x:
            x = x.replace(' ', ',')
        else:
            x = x.replace('\n', ',')
            x = x.replace(' ', '')
        x = x.strip().split("\n")
        x = [i.split(',') for i in x]
        x = x[0]
        # for i in x:
        #     i = i.replace(' ', ',')
        #     i = i.split(",")
        try:
            x.remove('')
        except:
            pass
        # print(x)
        co = [[float(x[0]), float(x[1])], [float(x[3]), float(x[4])],
              [float(x[6]), float(x[7])], [float(x[9]), float(x[10])]]
        coords.append(co)
        # y = str(place.ExtendedData.SchemaData.SimpleData)
        y = str(place.description)
        # print(y)
        # if it == 0:
        i = y.split("=")
        des = i[-1]
        des = des.split('<')
        des = des[0]
        des =des.strip()
        # else:
        #     des= y.strip()
        # print(des)
        # des = des.strip()
        ID.append(des)


    # ======================================== Change the Geo_projection of the Coordinated =============================
    inProj = Proj("+init=EPSG:4326", preserve_units=True)
    outProj = Proj("+init=EPSG:32615", preserve_units=True)

    global_coord_conv=[]
    for i in range(len(coords)):

        gcc1 = transform(inProj, outProj, coords[i][0][0], coords[i][0][1])
        gcc2 = transform(inProj, outProj, coords[i][1][0], coords[i][1][1])
        gcc3 = transform(inProj, outProj, coords[i][2][0], coords[i][2][1])
        gcc4 = transform(inProj, outProj, coords[i][3][0], coords[i][3][1])
        global_coord_conv.append([gcc1, gcc2, gcc3, gcc4])


    # ======================================== Load the image in GDAL and Get geoTransformation Values ==================

    srcImage = gdal.Open(file_paths[it])
    geoTrans = srcImage.GetGeoTransform()
    # ============================ Converting the Global Coordinates into the Pixel Coordinates ==========================

    pixel_coord =[]
    for i in range(len(global_coord_conv)):
        x,y = world2Pixel(geoTrans, global_coord_conv[i][0][0], global_coord_conv[i][0][1])
        g,h = world2Pixel(geoTrans, global_coord_conv[i][1][0], global_coord_conv[i][1][1])
        x1, y1 = world2Pixel(geoTrans, global_coord_conv[i][2][0], global_coord_conv[i][2][1])
        g1, h1 = world2Pixel(geoTrans, global_coord_conv[i][3][0], global_coord_conv[i][3][1])
        pixel_coord.append([(x, y), (g, h),(x1, y1), (g1, h1)])


    # ================================== Iterate the Boxes into the Image Processing ======================================

    # pixel_value = []
    ID_Value = []
    itervalue = []

    A=[]
    B=[]
    for i in range(len(pixel_coord)):
        # "Play Area is Here"
        try:
            x = [pixel_coord[i][0][0], pixel_coord[i][1][0], pixel_coord[i][2][0], pixel_coord[i][3][0]]
            y = [pixel_coord[i][0][1], pixel_coord[i][1][1], pixel_coord[i][2][1], pixel_coord[i][3][1]]
            x1, y1 = (min(x), min(y))
            x2, y2 = (max(x), max(y))
            crop_img = img[y1:y2, x1:x2]
            red = crop_img[:, :, 0]
            green = crop_img[:, :, 1]
            blue = crop_img[:, :, 2]

            gdivrb= np.mean(green)/(np.mean(red)+np.mean(blue))

            red_avg = np.mean(red)
            green_avg = np.mean(green)
            blue_avg = np.mean(blue)

            B.append(pixel_coord[i])
# Iteration 3 - SOY RM Prediction Methodology - 11June2019
            # if (red_avg > green_avg) and (green_avg + red_avg - blue_avg) > 0):
# Iteration 4 - SOY RM Prediction Methodology - 11June2019
#             if 2*green_avg < (red_avg+blue_avg) and red_avg > green_avg and (green_avg + red_avg - blue_avg) > 0:

# Iteration 5 - SOY RM Prediction Methodology - 11June2019
#             if 20*green_avg < 11*(red_avg+blue_avg):

            value = 0.55

            if gdivrb < value:
                A.append(pixel_coord[i])
                itervalue.append(i)
                ID_Value.append(ID[i])


            # green_float = img_as_float(crop_img)
            # vari = np.mean(green_float)
            # ID_Value.append(vari)

            # area = []
            # for n in range(len(contours)):
            #     area.append(cv2.contourArea(contours[n]))
            # max_area = sum(area)
            #
            # h, w, g = crop_img.shape
            # img_area = h * w
            # percentage = (max_area / img_area) * 100
            #

            #
            # if gdivrb < 0.55:
            #     A.append(pixel_coord[i])
            #     itervalue.append(i)
            #     ID_Value.append(ID[i])
        except:
            print('exception')


    A1 = [i for j, i in enumerate(B) if j not in itervalue]

    ID_Value_rem = [i for j, i in enumerate(ID) if j not in itervalue]

    v = Path(file_paths[it]).name
    v = v.replace(' ','_')
    file = v.replace(".", " ").split()[0]

    B = A

    Geo_Coord = []
    for i in range(len(B)):
        n, m = Pixel2world(geoTrans, B[i][0][0], B[i][0][1])
        o, p = Pixel2world(geoTrans, B[i][1][0], B[i][1][1])
        a, b = Pixel2world(geoTrans, B[i][2][0], B[i][2][1])
        c, d = Pixel2world(geoTrans, B[i][3][0], B[i][3][1])
        n, m = set_crs(n, m)
        o, p = set_crs(o, p)
        a, b = set_crs(a, b)
        c, d = set_crs(c, d)
        Geo_Coord.append([(n, m), (o, p), (a, b), (c, d)])


    kml = simplekml.Kml()
    i = 0
    for row in Geo_Coord:
        # print(row)
        pol = kml.newpolygon(outerboundaryis=[row[0], row[1], row[2], row[3], row[0]])
        pol.description = str(ID_Value[i])
        i+=1
    print("number of plots in "+ file + "is " , len(Geo_Coord))
    # v = Path(file_paths[it]).name
    # file = v.replace(".", " ").split()[0]
    kml.save('/test_container/Matured_Plots_KMLS/'+file+'.kml')


    Geo_Coord_RM = []
    for i in range(len(A1)):
        n, m = Pixel2world(geoTrans, A1[i][0][0], A1[i][0][1])
        o, p = Pixel2world(geoTrans, A1[i][1][0], A1[i][1][1])
        a, b = Pixel2world(geoTrans, A1[i][2][0], A1[i][2][1])
        c, d = Pixel2world(geoTrans, A1[i][3][0], A1[i][3][1])
        n, m = set_crs(n, m)
        o, p = set_crs(o, p)
        a, b = set_crs(a, b)
        c, d = set_crs(c, d)
        Geo_Coord_RM.append([(n,m),(o,p),(a,b),(c,d)])



    print('Remain Count is '+ str(len(Geo_Coord_RM)))

    kml = simplekml.Kml()
    i = 0
    for row in Geo_Coord_RM:
        # print(row)
        pol = kml.newpolygon(outerboundaryis=[row[0],row[1],row[2],row[3],row[0]])
        # print(row[0],row[1],row[2],row[3],row[0])
        pol.description = ID_Value_rem[i]
        i+=1

    from pathlib import Path
    v = Path(file_paths[it]).name
    file = v.replace(".", " ").split()[0]
    kml.save('/test_container/Source_KMLS/'+file+'_rem.kml')



end_time = datetime.now()
print('Total Duration of the Execution is {}'.format(end_time - start_time), "seconds")
